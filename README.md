# Básico do React Native 2

## Handling Text Input

`TextInput` é um componente básico que permite ao usuário inserir texto. Ele tem uma propriedade `onChangeText` que usa uma função para ser chamada toda vez que o texto é alterado, e `onSubmitEditing` que usa uma função para ser chamada quando o texto é submetido.

## Handling Touches

Os usuários interagem com aplicativos para dispositivos móveis principalmente por meio do toque. Eles podem usar uma combinação de gestos, como tocar em um botão, percorrer uma lista ou aplicar zoom em um mapa. O React Native fornece componentes para lidar com todos os tipos de gestos comuns, bem como um sistema abrangente de resposta a gestos para permitir um reconhecimento de gestos mais avançado, mas o componente nos interessa é o `Button`.

### Button

`Button` possui uma propriedade `onPress` que utiliza uma função para ser chamada toda vez que o botão for clicado, e `title` que define o conteúdo do botão.

Por padrão, um `Button` renderizará um renderizará um rótulo azul no iOS e um retângulo azul arredondado com texto branco no Android.

Você pode usar a propriedade `color` para alterar a propriedade do botão.

### Touchables

Você também pode criar seu próprio botão usando qualquer um dos componentes "Touchables (tocáveis)" fornecidos pelo React Native. Os componentes "Touchables" fornecem a capacidade de capturar gestos de toque e podem exibir feedback quando um gesto é reconhecido. No entanto, esses componentes não fornecem nenhum estilo padrão, por isso você precisará estilizar para que eles fiquem com boa aparência no seu aplicativo.

São eles:

- TouchableHighlight 
- TouchableNativeFeedback
- TouchableOpacity
- TouchableWithoutFeedback

Em alguns casos, você pode querer detectar quando um usuário pressiona e mantém uma visualização por um determinado período de tempo.
Essas prensas longas podem ser manipuladas passando uma função para a propriedade `onLongPress` de qualquer componente "Touchable".

## Using a ScrollView

O `ScrollView` é um contêiner de rolagem genérico que pode hospedar vários componentes e visualizações. Os itens roláveis ​​não precisam ser homogêneos e você pode rolar tanto vertical como na horizontal (definindo a propriedade `horizontal`).

O `ScrollView` funciona melhor para apresentar uma pequena quantidade de coisas de tamanho limitado. Todos os elementos e visualizações de um `ScrollView` são renderizados, mesmo que não sejam mostrados na tela. Se você tiver uma longa lista de itens que podem caber na tela, você deve usar um `FlatList`.

## Using List Views

O React Native fornece um conjunto de componentes para apresentar listas de dados. Geralmente, é usado `FlatList` ou `SectionList`.

### FlatList

O componente `FlatList` exibe uma lista de rolagem de dados alterados, mas de estrutura similar. O `FlatList` funciona bem para longas listas de dados, onde o número de itens pode mudar ao longo do tempo. Ao contrário do mais genérico `ScrollView`, o `FlatList` apenas processa elementos que estão sendo exibidos na tela, e não todos os elementos de uma só vez.

O `FlatList` requer duas propriedades: `data` e `renderItem`, onde `data` é a fonte de informações para a lista e `renderItem` pega um item da fonte e retorna um componente formatado para renderizar.

### SectionList

Usado para renderizar um conjunto de dados divididos em seções lógicas com cabeçalhos de seção.
Possui a propriedade `section`, que recebe um array de objetos com as propriedades `title`, `data`, `renderItem` e `renderSectionHeader`, onde `title` é o título da seção, `renderSectionHeader` renderiza o cabeçalho da secão e `data` e `renderItem` funcionam igual ao `FlatList`.

## Docs:
- [Handling Text Input](https://facebook.github.io/react-native/docs/handling-text-input)
- [Handling Touches](https://facebook.github.io/react-native/docs/handling-touches)
- [Using a ScrollView](https://facebook.github.io/react-native/docs/using-a-scrollview)
- [Using List Views](https://facebook.github.io/react-native/docs/using-a-listview)

## Desafio

Você deverá criar uma agenda de contatos, que recebe Nome, E-mail e Telefone.
O aplicativo deverá:

- Ter um campo para Nome, um campo para e-mail e um campo para Telefone.
- Ter um botão para gravar um contatos.
- Ter um botão para deletar um contato.
- Exibir os contatos cadastrados em uma lista ordenada em ordem alfabética.
- Validar se as informações foram preenchidas.
- Exibir todos os contatos.

### Inicialização:

- De um Fork no repositório: `https://gitlab.com/acct.fateclab/turma-1-sem-2019/07-basico-do-react-native-2`;
- Agora você deve clonar o repositório localmente, há um botão azul "Clone" no seu repositório do GitLab, clique nele e use a URL com HTTPS: `git clone url-copiada`;
- Agora dentro da pasta clonada, clique com o botão direito do mouse e clique em "Git Bash Here" para entrar na pasta pelo terminal;
- Para iniciar o servidor de desenvolvimento não se esqueça de usar `npm run start` e escanear o QRCode no aplicativo - Expo para que o App continue sendo buildado sempre que atualizar os arquivos.

### Entrega:

- Faça commits para cada forma diferente de exibição do texto;
- Assim que terminar dê `git push origin seu-nome/basico-do-react-native`, crie um Merge Request na interface do GitLab, acesse o menu "Merge Requests" e crie um, configure o "Target Branch" para o repositório original para que seu App seja avaliado e revisado e para que possamos te dar um feedback;
- O nome do Merge Request deve ser o seu nome completo.