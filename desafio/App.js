import React, { Component } from 'react'
import { StyleSheet, Text, View } from 'react-native'

import Example from './components/Example'

export default class App extends Component {
	render() {
		return (
			<View style={styles.container}>
				<Text>Open up App.js to start working on your app!</Text>
				<Example />
			</View>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 3,
		backgroundColor: '#fff',
		alignItems: 'center',
		justifyContent: 'center',
	},
})